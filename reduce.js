const each =require("./each.js");

function reduce(numbersArray, cb, startingValue) {

    
    if(Array.isArray(numbersArray)){
        if(numbersArray.length === 0){
            return("Data not found");
        }

        let accumulator = (startingValue !== undefined) ? startingValue : numbersArray.shift();

        each(numbersArray,(element)=>{
            accumulator=cb(accumulator,element)
        });
    
        return accumulator;
    }

    else{
        return("Passed data is not an array")
    }
}

module.exports=reduce;