
function each(numbersArray,cb){

    if(Array.isArray(numbersArray)){
        if(numbersArray.length === 0){
            return("Data not found");
        }

        for(let index in numbersArray){
            cb(numbersArray[index],index);
        }

    }
    else{
        return("Array is not present");
    }
    
}


module.exports=each;