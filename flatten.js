

function flatten(nestedArray){
    

    let flattenedArray=[];

    if(Array.isArray(nestedArray)){

        if(nestedArray.length === 0){
            return("Data not found");
        }

        nestedArray.forEach(function(element){

            if (Array.isArray(element)) {
                flattenedArray.push(...flatten(element));
            }
            else {
                flattenedArray.push(element);
            }

        });

        return flattenedArray;
    }
    else{
        return("Passed data is not an array")
    }

}

module.exports=flatten;