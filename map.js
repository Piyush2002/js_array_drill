
const each =require("./each.js");
function map(numbersArray,cb){

    

    const mapArray=[];

    if(Array.isArray(numbersArray)){
        if(numbersArray.length === 0){
            return("Data not found");
        }

        each(numbersArray,(element,index)=>{

            const result =cb(element,index);
            mapArray.push(result);
            
        });
    }

    else{
        return("Passed data is not an array")
    }

    return mapArray;
}


module.exports=map;