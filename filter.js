
const each =require("./each.js");

function filter(numbersArray,cb){

    

    const filteredArray=[];

    if(Array.isArray(numbersArray)){
        if(numbersArray.length === 0){
            return ("Data not found");
        }
        
        each(numbersArray,(element,index)=>{

            if(cb(element,index)){
                filteredArray.push(element);
            }

        });

        return filteredArray;
    }
    else{
        return("Passed data is not an array")
    }

}


module.exports=filter;